extern crate pixit;

use pixit::screen::Screen;
use pixit::gameresource::GameResource;
use pixit::animation::Animation;


fn main() {
    let mut resources = GameResource::new();
    let mut anim = Animation::new();
    let mut screen = Screen::new(1024, 768);
    resources.load_texture("grass", "examples/resources/grass_1flow.png");
    let texture = resources.get_texture("grass").unwrap();
    anim.push(0, 0, 32, 32);
    anim.push(32, 0, 32, 32);
    screen.create_entities(texture, &anim);
    screen.game_loop();
}
