extern crate sfml;

use std::{thread, time, vec};

use sfml::window::{ContextSettings, Event, Key, style, VideoMode};
use sfml::graphics::{Color, RenderWindow, RenderTarget, Texture};

use entity::Entity;
use animation::Animation;

pub struct Screen<'t> {
    window: RenderWindow,
    entities: vec::Vec<Entity<'t>>,
}


impl<'t> Screen<'t> {
    pub fn new(width: u32, height: u32) -> Self {
        let entities = vec::Vec::new();
        let settings = ContextSettings {
            antialiasing_level: 0,
            ..Default::default()
        };
        let video_mode = VideoMode::new(width, height, 32);
        let mut window = RenderWindow::new(
            video_mode,
            "DODHEIM",
            style::CLOSE,
            &settings
        ).unwrap();
        window.set_vertical_sync_enabled(true);
        Screen {
            window: window,
            entities: entities,
        }
    }

    pub fn tick(&mut self) -> bool {
        for event in self.window.events() {
            match event {
                Event::Closed |
                Event::KeyPressed { code: Key::Escape, .. } => return true,
                _ => {}
            }
        }
		self.window.clear(&Color::black());
        for ref mut entity in &mut self.entities {
            entity.animate();
            self.window.draw(&entity.sprite);
        }
        self.window.display();
        return false;
    }
    
    pub fn create_entities(&mut self, texture: &'t Texture, animation: &'t Animation) {
        for x in 0..32 {
            for y in 0..24 {
                let mut entity = Entity::new(texture, x as f32 * 32., y as f32 * 32.);
                entity.add_animation("blinking", animation);
                entity.select("blinking");
                self.entities.push(entity);
            }
        }
    }

    pub fn game_loop(&mut self) {
        let mili = time::Duration::from_millis(250);
        loop {
            if self.tick() {
                return
            }
            thread::sleep(mili);
        }
    }
}
