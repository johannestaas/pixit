//! 
//! The sprite wrapper module
//! 
extern crate sfml;

use std::collections::HashMap;
use self::sfml::system::Vector2;
use self::sfml::graphics::{Transformable, Texture, Sprite, IntRect};

use animation::Animation;


pub struct Entity<'t> {
    pub sprite: Sprite<'t>,
    animations: HashMap<&'t str, &'t Animation>,
    selection: Option<&'t Animation>,
    animation_i: i32,
}


impl<'t> Entity<'t> {
    pub fn new(texture: &'t Texture, x: f32, y: f32) -> Self {
        let mut sprite = Sprite::new();
        let pos = Vector2::new(x, y);
        sprite.set_texture(texture, false);
        sprite.set_position(&pos);
        Entity {
            sprite: sprite,
            animations: HashMap::new(),
            selection: None,
            animation_i: -1,
        }
    }

    pub fn set_rect(&mut self, x: i32, y: i32, w: i32, h: i32) {
        self.sprite.set_texture_rect(&IntRect::new(x, y, w, h));
    }

    pub fn animate(&mut self) {
        self.animation_i += 1;
        let sel = self.selection.unwrap().get_frame(self.animation_i);
        self.sprite.set_texture_rect(sel);
    }

    pub fn set_frame(&mut self, i: i32) {
        self.animation_i = i;
    }

    pub fn next_frame(&mut self) -> &IntRect {
        self.animation_i += 1;
        self.selection.unwrap().get_frame(self.animation_i)
    }

    pub fn get_selection(&self) -> &Animation {
        self.selection.unwrap()
    }

    pub fn get_frame(&self, i: i32) -> &IntRect {
        self.selection.unwrap().get_frame(i)
    }

    pub fn select(&mut self, selection: &'t str) {
        self.selection = Some(self.animations.get(selection).unwrap());
    }

    pub fn add_animation(&mut self, name: &'t str, animation: &'t Animation) {
        self.animations.insert(name, animation);
    }
}


