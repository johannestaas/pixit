//! 
//! The animation module that hosts 
//! 
extern crate sfml;

use std::vec;
use self::sfml::graphics::IntRect;

pub struct Animation {
    frames: vec::Vec<IntRect>,
}


impl<'t> Animation {
    pub fn new() -> Self {
        let frames = vec::Vec::new();
        Animation {
            frames: frames,
        }
    }

    pub fn push(&mut self, x: i32, y: i32, w: i32, h: i32) {
        self.frames.push(IntRect::new(x, y, w, h));
    }

    pub fn get_frame(&self, i: i32) -> &IntRect {
        let frame_num = i % self.frames.len() as i32;
        &self.frames[frame_num as usize]
    }
}
