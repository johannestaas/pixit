extern crate sfml;

use std::collections::HashMap;

use sfml::graphics::Texture;


pub struct GameResource<'t> {
    textures: HashMap<&'t str, Texture>,
}


impl<'t> GameResource<'t> {
    pub fn new() -> Self {
        GameResource {
            textures: HashMap::new(),
        }
    }
    
    pub fn load_texture(&mut self, name: &'t str, path: &str) {
        let texture = Texture::from_file(path).unwrap();
        self.textures.insert(name, texture);
    }

    pub fn get_texture(&mut self, name: &'t str) -> Option<&Texture> {
        self.textures.get(name)
    }
}
